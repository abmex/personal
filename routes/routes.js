import { Router }  from 'express';

const routes  = Router();

routes.get('*', (req, res) => {
  res.sendFile("index.html", {root: "./build"});
});

export default routes
