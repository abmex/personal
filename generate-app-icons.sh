#!/usr/bin/env bash

set -e

SRC='./app/images/icons/'
DEST='./build/'

npx sharp -i ./app/images/icons/icon_512x512.png -o ./build/favicon-16x16.png resize 16
npx sharp -i ./app/images/icons/icon_512x512.png -o ./build/favicon-32x32.png resize 32
npx sharp -i ./app/images/icons/icon_512x512.png -o ./build/favicon-96x96.png resize 96
npx sharp -i ./app/images/icons/icon_512x512.png -o ./build/icon-512x512.png resize 512
npx sharp -i ./app/images/icons/icon_512x512.png -o ./build/icon-384x384.png resize 384
npx sharp -i ./app/images/icons/icon_512x512.png -o ./build/icon-256x256.png resize 256
npx sharp -i ./app/images/icons/icon_512x512.png -o ./build/icon-192x192.png resize 192
npx sharp -i ./app/images/icons/icon_512x512.png -o ./build/icon-144x144.png resize 144
npx sharp -i ./app/images/icons/icon_512x512.png -o ./build/icon-96x96.png resize 96
npx sharp -i ./app/images/icons/icon_512x512.png -o ./build/icon-48x48.png resize 48
npx sharp -i ./app/images/icons/icon_512x512.png -o ./build/apple-icon-180x180.png resize 180
npx sharp -i ./app/images/icons/icon_512x512.png -o ./build/apple-icon-152x152.png resize 152
npx sharp -i ./app/images/icons/icon_512x512.png -o ./build/apple-icon-144x144.png resize 144
npx sharp -i ./app/images/icons/icon_512x512.png -o ./build/apple-icon-120x120.png resize 120
npx sharp -i ./app/images/icons/icon_512x512.png -o ./build/apple-icon-114x114.png resize 114
npx sharp -i ./app/images/icons/icon_512x512.png -o ./build/apple-icon-76x76.png resize 76
npx sharp -i ./app/images/icons/icon_512x512.png -o ./build/apple-icon-72x72.png resize 72
npx sharp -i ./app/images/icons/icon_512x512.png -o ./build/apple-icon-60x60.png resize 60
npx sharp -i ./app/images/icons/icon_512x512.png -o ./build/apple-icon-57x57.png resize 57
