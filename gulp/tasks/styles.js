import config       from '../config';
import handleErrors from '../util/handleErrors';
import gulp         from 'gulp';
import gulpif       from 'gulp-if';
import newer        from 'gulp-newer';
import sourcemaps   from 'gulp-sourcemaps';
import sass         from 'gulp-sass';
import browserSync  from 'browser-sync';
import autoprefixer from 'gulp-autoprefixer';



gulp.task('styles', () => {

  const createSourcemap = !global.isProd || config.styles.prodSourcemap;

  return gulp.src(config.styles.src)
    .pipe(gulpif(createSourcemap, sourcemaps.init()))
    .pipe(sass({
      sourceComments: !global.isProd,
      outputStyle: global.isProd ? 'compressed' : 'nested',
      includePaths: config.styles.sassIncludePaths
    }))
    .on('error', handleErrors)
    .pipe(autoprefixer({
      browsers: ['last 3 versions', '> 1%', 'ie 8']
    }))
    .pipe(gulpif(
      createSourcemap,
      sourcemaps.write( global.isProd ? './' : null ))
    )
    .pipe(gulp.dest(config.styles.dest))
    .on('error', handleErrors)
    .pipe(browserSync.stream());

});
