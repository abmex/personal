import gulp    from 'gulp';
import path    from 'path';
import fs      from 'fs';
import config  from '../config';
gulp.task('backup', function() {

    if (! fs.existsSync(path.join(__dirname, `../../${config.buildBackupDir}`))) {
        fs.mkdirSync(path.join(__dirname, `../../${config.buildBackupDir}`));
        console.log(`       ** Created backup dir: ${config.buildBackupDir}`);
    }

    return gulp.src(`../../${config.buildDir}`)
        .pipe(gulp.dest(`../../${config.buildBackupDir}`));

});