document.addEventListener('DOMContentLoaded', (event) => {
  console.log('in document.DOM content');
  console.log('');

  const vpHeight = getStyleValue('.left-panel', 'height');
//  document.querySelector('.left-panel:before').style.height = vpHeight
  console.log(`viewport height: ${vpHeight}`);

  console.log(`window x: ${window.innerWidth}`);
  console.log(`window y: ${window.innerHeight}`);
  console.log('');

  console.log(`screen x: ${screen.width}`);
  console.log(`screen y: ${screen.height}`);
  console.log('');

  console.log(`mode: ${process.env}`);

  console.log(`.wrapper x: ${getStyleValue('.left-panel', 'width')}`);
  console.log(`.wrapper y: ${getStyleValue('.left-panel', 'height')}`);
  console.log(`.wrapper padding: ${getStyleValue('.left-panel', 'padding-bottom')}`);
  // console.log(`.svg x: ${getStyleValue('.svg', 'width')}`);
  // console.log(`.svg y: ${getStyleValue('.svg',  'height')}`);
  console.log('---------------------------------------------------');

  window.addEventListener('orientationchange', (event) => {
    console.log(`window x: ${window.innerWidth}`);
    console.log(`window y: ${window.innerHeight}`);
    document.querySelector('html').style.zoom = '100%';
  }, false);


  window.addEventListener('resize', (event) => {
    console.log(`font size: ${getStyleValue('li:first-child','font-size')}`);
    console.log('---------------------------------------------------');
  }, false);

}, false);

const getStyleValue = (elem, style) =>  {
  return window.getComputedStyle(document.querySelector(elem, null)).getPropertyValue(style);
}
