import express from 'express';
import path from 'path';
import logger from 'morgan'
import routes from './routes/routes';
import bodyParser from 'body-parser';
import methodOverride from 'method-override'

const app = express();
const env = process.env.NODE_ENV || 'development';
const port = process.env.NODE_SERVER_PORT || 1337;

app.set('views', __dirname + '/views');
app.set('port', process.env.NODE_SERVER_PORT || 1337);

app.use(logger('dev'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(methodOverride());
app.use(express.static(path.join(__dirname, 'build')));

app.use('/', routes);

//app.use(favicon(path.join(__dirname + '/favicon.ico')));

const server = app.listen(port, () => {
  console.log(`\nExpress listening on port ${port}...\n`);
})
